<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateIndependentKeysName extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('independent_keys', function(Blueprint $table)
		{
			$table->string('name', 128)->after('serial_key')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('independent_keys', function(Blueprint $table)
		{
			$table->dropColumn('name');
		});
	}

}
