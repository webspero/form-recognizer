<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTblcompetitorPhoneEmail extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tblcompetitor', function(Blueprint $table)
		{
			$table->string('competitor_phone', 128)->nullable()->after('competitor_contact_details');
			$table->string('competitor_email')->nullable()->after('competitor_contact_details');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblcompetitor', function(Blueprint $table)
		{
			$table->dropColumn('competitor_phone');
			$table->dropColumn('competitor_email');
		});
	}

}
