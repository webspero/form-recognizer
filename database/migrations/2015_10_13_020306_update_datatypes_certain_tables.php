<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateDatatypesCertainTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::statement('ALTER TABLE `tblcapacityexpansion` MODIFY `current_year` decimal(15,2) NOT NULL;');
		DB::statement('ALTER TABLE `tblcapacityexpansion` MODIFY `new_year` decimal(15,2) NOT NULL;');
		DB::statement('ALTER TABLE `tblcapacityexpansion` MODIFY `within_five_years` decimal(15,2) NULL;');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
