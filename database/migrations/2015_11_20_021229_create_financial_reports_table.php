<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFinancialReportsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('financial_reports', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title');
			$table->string('year', 4);
			$table->integer('industry_id');
			$table->string('money_factor', 20);
			$table->string('currency', 3);
			$table->smallInteger('step');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('financial_reports');
	}

}
