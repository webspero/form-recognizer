<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTlbSmeScore extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tblsmescore', function(Blueprint $table)
		{
			$table->increments('smescoreid');
			$table->integer('loginid')->index();
			$table->integer('entityid')->index();
			$table->integer('score_rm');
			$table->integer('score_cd');
			$table->integer('score_sd');
			$table->integer('score_bois');
			$table->integer('score_boe');
			$table->integer('score_mte');
			$table->integer('score_bdms');
			$table->integer('score_sp');
			$table->integer('score_ppfi');
			$table->float('score_rfp');
			$table->float('score_rfpm');
			$table->float('score_fars');
			$table->integer('score_facs');
			$table->string('score_sf',100);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tblsmescore');
	}

}
