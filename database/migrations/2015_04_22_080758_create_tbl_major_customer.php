<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblMajorCustomer extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tblmajorcustomer', function(Blueprint $table)
		{
			$table->increments('majorcustomerrid');
			$table->integer('entity_id');
			$table->integer('loginid')->index();
			$table->string('customer_name',200);
			$table->string('customer_share_sales',200);
			$table->string('customer_address',200);
			$table->string('customer_contact_person',200);
			$table->string('customer_contact_details',200);
			$table->integer('customer_experience');
			$table->integer('customer_started_years');
			$table->string('customer_years_doing_business',200);
			$table->string('customer_settlement',200);
			$table->string('customer_order_frequency',200);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tblmajorcustomer');
	}

}
