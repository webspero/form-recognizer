<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCurrenciesRange extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::statement('ALTER TABLE `tbladverserecord` MODIFY `adverserecord_dueamount` decimal(15,2) NOT NULL;');
		DB::statement('ALTER TABLE `tblcapital` MODIFY `capital_authorized` decimal(15,2) NOT NULL;');
		DB::statement('ALTER TABLE `tblcapital` MODIFY `capital_issued` decimal(15,2) NOT NULL;');
		DB::statement('ALTER TABLE `tblcapital` MODIFY `capital_paid_up` decimal(15,2) NOT NULL;');
		DB::statement('ALTER TABLE `tblcapital` MODIFY `capital_ordinary_shares` decimal(15,2) NOT NULL;');
		DB::statement('ALTER TABLE `tblcapital` MODIFY `capital_par_value` decimal(15,2) NOT NULL;');
		DB::statement('ALTER TABLE `tblcreditlines` MODIFY `bank_initial_availment` decimal(15,2) NOT NULL;');
		DB::statement('ALTER TABLE `tblcreditlines` MODIFY `bank_outstanding_amount` decimal(15,2) NOT NULL;');
		DB::statement('ALTER TABLE `tblcreditlines` MODIFY `bank_volume_trade` decimal(15,2) NOT NULL;');
		DB::statement('ALTER TABLE `tblfuturegrowth` MODIFY `futuregrowth_estimated_cost` decimal(15,2) NOT NULL;');
		DB::statement('ALTER TABLE `tblfuturesource` MODIFY `opg_per_year` decimal(15,2) NOT NULL;');
		DB::statement('ALTER TABLE `tblfuturesource` MODIFY `family_per_year` decimal(15,2) NOT NULL;');
		DB::statement('ALTER TABLE `tblfuturesource` MODIFY `bp_per_year` decimal(15,2) NOT NULL;');
		DB::statement('ALTER TABLE `tblfuturesource` MODIFY `vcc_per_year` decimal(15,2) NOT NULL;');
		DB::statement('ALTER TABLE `tblfuturesource` MODIFY `ipo_per_year` decimal(15,2) NOT NULL;');
		DB::statement('ALTER TABLE `tblfuturesource` MODIFY `others_per_year` decimal(15,2) NOT NULL;');
		DB::statement('ALTER TABLE `tblindustry_main` MODIFY `net_cash` decimal(15,2) NOT NULL;');
		DB::statement('ALTER TABLE `tblinsurance` MODIFY `insured_amount` decimal(15,2) NOT NULL;');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// no down, this is required
	}

}
