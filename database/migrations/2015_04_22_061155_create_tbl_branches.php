<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblBranches extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tblbranches', function(Blueprint $table)
		{
			$table->increments('branchid');
			$table->integer('entity_id');
			$table->integer('loginid')->index();
			$table->string('branch_address',200);
                        $table->string('branch_owned_leased',200);
			$table->string('branch_contact_person',200);
			$table->string('branch_contact_details',200);
			$table->string('branch_job_title',200);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tblbranches');
	}

}
