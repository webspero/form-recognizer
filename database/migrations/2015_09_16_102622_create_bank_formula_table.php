<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankFormulaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bank_formula', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('bank_id');
			$table->string('bcc', 30);
			$table->string('mq', 30);
			$table->string('fa', 30);
			$table->smallInteger('boost')->default(1);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bank_formula');
	}

}
