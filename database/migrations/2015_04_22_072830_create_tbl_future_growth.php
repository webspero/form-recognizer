<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblFutureGrowth extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tblfuturegrowth', function(Blueprint $table)
		{
			$table->increments('futuregrowthid');
			$table->integer('entity_id');
			$table->integer('loginid')->index();
			$table->string('futuregrowth_name',200);
			$table->bigInteger('futuregrowth_estimated_cost');
			$table->string('futuregrowth_implementation_date',200);
			$table->string('futuregrowth_source_capital',200);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tblfuturegrowth');
	}

}
