<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTblmajorsupplierTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tblmajorsupplier', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `tblmajorsupplier` MODIFY `supplier_share_sales` int(3) NOT NULL;');
			DB::statement('ALTER TABLE `tblmajorsupplier` MODIFY `supplier_years_doing_business` int(3) NOT NULL;');
			DB::statement('ALTER TABLE `tblmajorsupplier` MODIFY `supplier_settlement` int(3) NOT NULL;');
			DB::statement('ALTER TABLE `tblmajorsupplier` MODIFY `supplier_order_frequency` varchar(30) NOT NULL;');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblmajorsupplier', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `tblmajorsupplier` MODIFY `supplier_share_sales` varchar(200) NOT NULL;');
			DB::statement('ALTER TABLE `tblmajorsupplier` MODIFY `supplier_years_doing_business` varchar(200) NOT NULL;');
			DB::statement('ALTER TABLE `tblmajorsupplier` MODIFY `supplier_settlement` varchar(200) NOT NULL;');
			DB::statement('ALTER TABLE `tblmajorsupplier` MODIFY `supplier_order_frequency` varchar(200) NOT NULL;');
		});
	}

}
