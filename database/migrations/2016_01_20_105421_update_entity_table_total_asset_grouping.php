<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateEntityTableTotalAssetGrouping extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tblentity', function(Blueprint $table)
		{
			$table->smallInteger('total_asset_grouping');
			$table->string('potential_credit_quality_issues', 20);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblentity', function(Blueprint $table)
		{
			$table->dropColumn('total_asset_grouping');
			$table->dropColumn('potential_credit_quality_issues');
		});
	}

}
