<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCertificationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tblcertifications', function($table)
		{
			$table->string('certification_reg_no', 45);
			$table->date('certification_reg_date');
			$table->string('certification_doc', 45);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblcertifications', function($table)
		{
			$table->dropColumn('certification_reg_no');
			$table->dropColumn('certification_reg_date');
			$table->dropColumn('certification_doc');
		});
	}

}
