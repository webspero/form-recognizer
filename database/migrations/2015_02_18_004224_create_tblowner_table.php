<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblownerTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tblowner', function(Blueprint $table)
		{
			$table->increments('ownerid');
			$table->integer('entity_id');
			$table->integer('loginid')->index();
			$table->string('firstname', 100);
			$table->string('middlename', 100);
			$table->string('lastname', 100);
			$table->string('nationality', 100);
			$table->date('birthdate');
			$table->string('civilstatus', 50);
			$table->string('profession', 200);
			$table->string('email')->index();
			$table->string('address1', 200);
			$table->string('address2', 200);
			$table->integer('cityid')->index();
			$table->string('province', 100);
			$table->integer('zipcode');
			$table->integer('present_address_status');
			$table->integer('no_yrs_present_address');
			$table->integer('phone');
			$table->integer('tin_num');
			$table->integer('percent_of_ownership');
			$table->integer('number_of_year_engaged');
			$table->string('position', 200);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tblowner');
	}

}
