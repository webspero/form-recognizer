<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblBusinesssDriver extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tblbusinessdriver', function(Blueprint $table)
		{
			$table->increments('businessdriverid');
			$table->integer('entity_id');
			$table->integer('loginid')->index();
			$table->string('businessdriver_name',200);
			$table->string('businessdriver_total_sales',200);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tblbusinessdriver');
	}

}
