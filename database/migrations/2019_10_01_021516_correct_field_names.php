<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CorrectFieldNames extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tblshareholders', function (Blueprint $table) {
            $table->renameColumn('birthDate', 'birthdate');
            $table->renameColumn('lastName', 'lastname');
            $table->renameColumn('middleName', 'middlename');
            $table->renameColumn('firstName', 'firstname');
        });

        Schema::table('tbldirectors', function (Blueprint $table) {
            $table->renameColumn('birthDate', 'birthdate');
            $table->renameColumn('lastName', 'lastname');
            $table->renameColumn('middleName', 'middlename');
            $table->renameColumn('firstName', 'firstname');
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tblshareholders', function (Blueprint $table) {
            $table->renameColumn('birthdate', 'birthDate');
            $table->renameColumn('lastname', 'lastName');
            $table->renameColumn('middlename', 'middleName');
            $table->renameColumn('firstname', 'firstName');
        });

        Schema::table('tbldirectors', function (Blueprint $table) {
            $table->renameColumn('birthdate', 'birthDate');
            $table->renameColumn('lastname', 'lastName');
            $table->renameColumn('middlename', 'middleName');
            $table->renameColumn('firstname', 'firstName');
        });
    }
}
