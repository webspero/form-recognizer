<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblScoring extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tblscoring', function(Blueprint $table)
		{
			$table->increments('scoringid');
			$table->integer('loginid')->index();
			$table->integer('entityid')->index();
			$table->date('start_date')->nullable();
			$table->date('end_date');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tblscoring');
	}

}
