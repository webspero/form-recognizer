<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAdverseRecord extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tbladverserecord', function(Blueprint $table)
		{
			$table->increments('adverserecordid');
			$table->integer('entity_id');
			$table->integer('loginid')->index();
			$table->string('adverserecord_name',200);
			$table->string('adverserecord_products',200);
			$table->date('adverserecord_pastdue');
			$table->integer('adverserecord_dueamount');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tbladverserecord');
	}

}
