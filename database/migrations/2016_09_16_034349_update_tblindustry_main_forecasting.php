<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTblindustryMainForecasting extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tblindustry_main', function(Blueprint $table)
		{
			$table->decimal('q1', 20, 6);
			$table->decimal('q2', 20, 6);
			$table->decimal('q3', 20, 6);
			$table->decimal('q4', 20, 6);
			$table->decimal('q5', 20, 6);
			$table->decimal('q6', 20, 6);
			$table->decimal('q7', 20, 6);
			$table->decimal('q8', 20, 6);
			$table->decimal('slope', 15, 6);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblindustry_main', function(Blueprint $table)
		{
			$table->dropColumn('q1');
			$table->dropColumn('q2');
			$table->dropColumn('q3');
			$table->dropColumn('q4');
			$table->dropColumn('q5');
			$table->dropColumn('q6');
			$table->dropColumn('q7');
			$table->dropColumn('q8');
			$table->dropColumn('slope');
		});
	}

}
