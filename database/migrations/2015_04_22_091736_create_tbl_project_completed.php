<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblProjectCompleted extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tblprojectcompleted', function(Blueprint $table)
		{
			$table->increments('projectcompletedid');
			$table->integer('entity_id');
			$table->integer('loginid')->index();
			$table->string('projectcompleted_name',200);
			$table->bigInteger('projectcompleted_cost');
			$table->string('projectcompleted_source_funding',200);
			$table->tinyInteger('projectcompleted_year_began');
			$table->string('projectcompleted_result',100);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tblprojectcompleted');
	}

}
