<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BspDownloadReport extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bsp_expectation_downloads', function(Blueprint $table)
        {
            $table->increments('bsp_file_id');
            $table->string('filename', 45);
            $table->tinyInteger('status');
            $table->date('date_uploaded');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bsp_expectation_downloads');
	}

}
