<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateBankStandalone extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tblbank', function(Blueprint $table)
		{
			$table->smallInteger('is_standalone')->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblbank', function(Blueprint $table)
		{
			$table->dropColumn('is_standalone');
		});
	}

}
