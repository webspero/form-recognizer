<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaypalTransactions extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('paypal_transactions', function(Blueprint $table) {
			$table->increments('id');
			$table->string('cancel_url', 250);
			$table->string('return_url', 250);
			$table->string('name', 50);
			$table->string('description', 50);
			$table->float('amount');
			$table->string('currency', 10);
			$table->integer('reference_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('paypal_transactions');
	}

}
