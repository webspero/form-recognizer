<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCycleDetailsDatatype extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tblcycledetails', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `tblcycledetails` MODIFY `credit_sales` decimal(15,2) NULL;');
			DB::statement('ALTER TABLE `tblcycledetails` MODIFY `accounts_receivable` decimal(15,2) NULL;');
			DB::statement('ALTER TABLE `tblcycledetails` MODIFY `inventory` decimal(15,2) NULL;');
			DB::statement('ALTER TABLE `tblcycledetails` MODIFY `cost_of_sales` decimal(15,2) NULL;');
			DB::statement('ALTER TABLE `tblcycledetails` MODIFY `accounts_payable` decimal(15,2) NULL;');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblcycledetails', function(Blueprint $table)
		{
			//
		});
	}

}
