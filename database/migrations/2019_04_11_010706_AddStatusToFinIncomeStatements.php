<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusToFinIncomeStatements extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::table('fin_income_statements', function(Blueprint $table)
		{
			$table->integer('is_deleted')->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::table('fin_income_statements', function(Blueprint $table)
		{
			$table->dropColumn('is_deleted');
		});
	}

}
