<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableTblentityForMatching extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tblentity', function (Blueprint $table) {
            $table->enum('matching_optin',['Yes','No','Pending'])->default('Pending');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tblentity', function (Blueprint $table) {
            $table->dropColumn('matching_optin');
        });
    }
}
