<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNotifSentToTblemailnotif extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tblemailnotif', function (Blueprint $table) {
            $table->tinyInteger('notif_reports_started_sent')->after('status');
            $table->tinyInteger('notif_reports_completed_sent')->after('notif_reports_started_sent');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tblemailnotif', function (Blueprint $table) {
            $table->dropColumn('notif_reports_started_sent');
            $table->dropColumn('notif_reports_completed_sent');
        });
    }
}
