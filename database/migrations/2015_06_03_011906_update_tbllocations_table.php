<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTbllocationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tbllocations', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `tbllocations` MODIFY `location_type` varchar(50) NOT NULL;');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tbllocations', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `tbllocations` MODIFY `location_type` varchar(200) NOT NULL;');
		});
	}

}
