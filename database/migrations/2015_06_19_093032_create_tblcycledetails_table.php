<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblcycledetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tblcycledetails', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('entity_id');
			$table->integer('loginid')->index();
			$table->smallInteger('index');
			$table->decimal('credit_sales');
			$table->decimal('accounts_receivable');
			$table->decimal('inventory');
			$table->decimal('cost_of_sales');
			$table->decimal('accounts_payable');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tblcycledetails');
	}

}
