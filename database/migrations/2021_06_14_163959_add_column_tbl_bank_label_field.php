<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnTblBankLabelField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tblbank', function (Blueprint $table) {
            $table->integer('isLabel')->default(0);
            $table->string('white_label_name');
            $table->string('brand_img');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tblbank', function (Blueprint $table) {
            //
        });
    }
}
