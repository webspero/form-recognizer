<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTransactionsController extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tbltransactions', function(Blueprint $table)
		{
			$table->integer('entity_id')->nullable()->after('loginid');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tbltransactions', function(Blueprint $table)
		{
			$table->dropColumn('entity_id');
		});
	}

}
