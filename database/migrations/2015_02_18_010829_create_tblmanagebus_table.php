<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblmanagebusTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tblmanagebus', function(Blueprint $table)
		{
			$table->increments('managebusid');
			$table->integer('entity_id');
			$table->integer('ownerid')->index();
			$table->integer('loginid')->index();
			$table->string('bus_name', 200);
			$table->string('bus_type', 100);
			$table->integer('bus_location')->index();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tblmanagebus');
	}

}
