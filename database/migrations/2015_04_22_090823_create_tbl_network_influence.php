<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblNetworkInfluence extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tblnetworkinfluence', function(Blueprint $table)
		{
			$table->increments('networkinfluenceid');
			$table->integer('ownerid')->index();
			$table->integer('loginid')->index();
			$table->string('network_name',200);
			$table->string('network_type',100);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tblnetworkinfluence');
	}

}
