<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTblfuturesourceTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tblfuturesource', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `tblfuturesource` MODIFY `opg_per_year` decimal(10,2) NOT NULL;');
			DB::statement('ALTER TABLE `tblfuturesource` MODIFY `family_per_year` decimal(10,2) NOT NULL;');
			DB::statement('ALTER TABLE `tblfuturesource` MODIFY `bp_per_year` decimal(10,2) NOT NULL;');
			DB::statement('ALTER TABLE `tblfuturesource` MODIFY `vcc_per_year` decimal(10,2) NOT NULL;');
			DB::statement('ALTER TABLE `tblfuturesource` MODIFY `ipo_per_year` decimal(10,2) NOT NULL;');
			DB::statement('ALTER TABLE `tblfuturesource` MODIFY `others_per_year` decimal(10,2) NOT NULL;');
			DB::statement('ALTER TABLE `tblfuturesource` MODIFY `opg_horizontal_year` int(4) NOT NULL;');
			DB::statement('ALTER TABLE `tblfuturesource` MODIFY `family_horizon_year` int(4) NOT NULL;');
			DB::statement('ALTER TABLE `tblfuturesource` MODIFY `bp_horizon_year` int(4) NOT NULL;');
			DB::statement('ALTER TABLE `tblfuturesource` MODIFY `vcc_horizon_year` int(4) NOT NULL;');
			DB::statement('ALTER TABLE `tblfuturesource` MODIFY `ipo_horizon_year` int(4) NOT NULL;');
			DB::statement('ALTER TABLE `tblfuturesource` MODIFY `others_horizon_year` int(4) NOT NULL;');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblfuturesource', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `tblfuturesource` MODIFY `opg_per_year` varchar(255) NOT NULL;');
			DB::statement('ALTER TABLE `tblfuturesource` MODIFY `family_per_year` varchar(255) NOT NULL;');
			DB::statement('ALTER TABLE `tblfuturesource` MODIFY `bp_per_year` varchar(255) NOT NULL;');
			DB::statement('ALTER TABLE `tblfuturesource` MODIFY `vcc_per_year` varchar(255) NOT NULL;');
			DB::statement('ALTER TABLE `tblfuturesource` MODIFY `ipo_per_year` varchar(255) NOT NULL;');
			DB::statement('ALTER TABLE `tblfuturesource` MODIFY `others_per_year` varchar(255) NOT NULL;');
			DB::statement('ALTER TABLE `tblfuturesource` MODIFY `opg_horizontal_year` varchar(255) NOT NULL;');
			DB::statement('ALTER TABLE `tblfuturesource` MODIFY `family_horizon_year` varchar(255) NOT NULL;');
			DB::statement('ALTER TABLE `tblfuturesource` MODIFY `bp_horizon_year` varchar(255) NOT NULL;');
			DB::statement('ALTER TABLE `tblfuturesource` MODIFY `vcc_horizon_year` varchar(255) NOT NULL;');
			DB::statement('ALTER TABLE `tblfuturesource` MODIFY `ipo_horizon_year` varchar(255) NOT NULL;');
			DB::statement('ALTER TABLE `tblfuturesource` MODIFY `others_horizon_year` varchar(255) NOT NULL;');
		});
	}

}
