<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBspExpectations extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bsp_business_expectations', function(Blueprint $table)
        {
            $table->increments('bsp_expectation_id');
            $table->integer('industry_id');
            $table->string('filename', 45);
            $table->tinyInteger('status');
            $table->date('date_uploaded');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bsp_business_expectations');
	}

}
