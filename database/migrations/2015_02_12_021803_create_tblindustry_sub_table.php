<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblindustrySubTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tblindustry_sub', function(Blueprint $table)
		{
			$table->increments('industry_sub_id');
			$table->integer('industry_main_id')->index();
			$table->string('sub_code', 100);
			$table->string('sub_title', 200);
			$table->integer('sub_status')->index();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tblindustry_sub');
	}

}
