<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblspouseTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tblspouse', function(Blueprint $table)
		{
			$table->increments('spouseid');
			$table->integer('entity_id');
			$table->integer('ownerid')->index();
			$table->integer('loginid')->index();
			$table->string('firstname', 100);
			$table->string('middlename', 100);
			$table->string('lastname', 100);
			$table->string('nationality', 100);
			$table->date('birthdate');
			$table->string('profession', 200);
			$table->string('email')->index();
			$table->integer('phone');
			$table->integer('tin_num');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tblspouse');
	}

}
