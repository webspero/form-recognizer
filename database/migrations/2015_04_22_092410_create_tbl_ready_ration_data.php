<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblReadyRationData extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tblreadyrationdata', function(Blueprint $table)
		{
			$table->increments('readyrationdataid');
			$table->integer('loginid')->index();
			$table->integer('entityid')->index();
			$table->integer('year1');
			$table->integer('year2');
			$table->double('gross_revenue_growth1',10,2);
			$table->double('gross_revenue_growth2',10,2);
			$table->double('net_income_growth1',10,2);
			$table->double('net_income_growth2',10,2);
			$table->double('gross_profit_margin1',10,2);
			$table->double('gross_profit_margin2',10,2);
			$table->double('net_profit_margin1',10,2);
			$table->double('net_profit_margin2',10,2);
			$table->double('net_cash_margin1',10,2);
			$table->double('net_cash_margin2',10,2);
			$table->double('current_ratio1',10,2);
			$table->double('current_ratio2',10,2);
			$table->double('debt_equity_ratio1',10,2);
			$table->double('debt_equity_ratio2',10,2);
			$table->bigInteger('receivables_turnover1');
			$table->bigInteger('receivables_turnover2');
			$table->bigInteger('inventory_turnover1');
			$table->bigInteger('inventory_turnover2');
			$table->bigInteger('accounts_payable_turnover1');
			$table->bigInteger('accounts_payable_turnover2');
			$table->bigInteger('cash_conversion_cycle1');
			$table->bigInteger('cash_conversion_cycle2');
			$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
			$table->timestamp('updated_at');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tblreadyrationdata');
	}

}
