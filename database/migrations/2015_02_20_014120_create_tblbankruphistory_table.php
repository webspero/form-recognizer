<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblbankruphistoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tblbankruphistory', function(Blueprint $table)
		{
			$table->increments('bankruphistoryid');
			$table->integer('ownerid')->index();
			$table->integer('loginid')->index();
			$table->string('bank_history', 100);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tblbankruphistory');
	}

}
