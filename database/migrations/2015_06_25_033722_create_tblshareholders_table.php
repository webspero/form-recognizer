<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblshareholdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tblshareholders', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('entity_id');
			$table->integer('loginid');
			$table->string('name');
			$table->string('address', 512);
			$table->decimal('amount', 15, 2);
			$table->float('percentage_share');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tblshareholders');
	}

}
