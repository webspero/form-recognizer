<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblplanfacilityTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tblplanfacility', function(Blueprint $table)
		{
			$table->increments('planfacilityid');
			$table->integer('entity_id');
			$table->integer('loginid')->index();
			$table->string('plan_credit_availment');
			$table->string('cash_deposit_details');
			$table->string('cash_deposit_amount');
			$table->string('securities_details');
			$table->string('securities_estimated_value');
			$table->string('property_details');
			$table->string('property_estimated_value');
			$table->string('chattel_details');
			$table->string('chattel_estimated_value');
			$table->string('others_details');
			$table->string('others_estimated_value');
			$table->string('purpose_credit_facility');
			$table->string('purpose_credit_facility_others');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tblplanfacility');
	}

}
