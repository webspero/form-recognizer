<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnIndustryMainCode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('tblindustry_main', function(Blueprint $table)
		{
			$table->string('main_code', 1)->after('industry_main_id');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('tblindustry_main', function(Blueprint $table)
		{
			$table->dropColumn('industrymain_code');
		});
    }
}
