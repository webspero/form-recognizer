<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblentityTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tblentity', function(Blueprint $table)
		{
			$table->increments('entityid');
			$table->integer('loginid')->index();
			$table->string('companyname', 100);
			$table->integer('industry_main_id')->index();
			$table->integer('industry_sub_id')->index();
			$table->integer('industry_row_id')->index();
			$table->bigInteger('total_assets');
			$table->string('website', 100);
			$table->string('email', 75);
			$table->string('address1', 200);
			$table->string('address2', 200);
			$table->integer('cityid')->index();
			$table->string('province', 100);
			$table->integer('zipcode');
			$table->string('phone',100);
			$table->string('former_address1',200);
			$table->string('former_address2',200);
			$table->string('former_cityid',200);
			$table->string('former_province',100); 
			$table->integer('former_zipcode');
			$table->string('former_phone',100);
			$table->integer('no_yrs_present_address');
			$table->date('date_established');
			$table->integer('number_year');
			$table->longText('description');
			$table->integer('employee_size');
			$table->integer('number_year_management_team');
			$table->string('related_companies', 100);
			$table->string('tin_num', 100);
			$table->string('sec_num', 100);
			$table->integer('is_agree')->index();
			$table->integer('is_subscribe')->index();
			$table->integer('status')->index();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tblindustry_row');
	}

}
