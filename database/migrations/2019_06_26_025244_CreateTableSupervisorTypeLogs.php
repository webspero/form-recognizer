<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSupervisorTypeLogs extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('supervisor_type_update_logs', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer("is_standalone");
			$table->integer("bank_id");
			$table->integer('updated_by');
			$table->timestamps();
			$table->integer("is_deleted");
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop("supervisor_type_update_logs");
	}

}
