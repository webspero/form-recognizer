<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Add2faAttempt extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_2fa_attempts', function(Blueprint $table)
        {
            $table->increments('user_attempt_id');
            $table->integer('login_id');
            $table->string('image', 45);
            $table->string('passcode', 120);
            $table->integer('attempts');
            $table->tinyInteger('status');
            $table->datetime('last_attempt');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_2fa_attempts');
	}

}
