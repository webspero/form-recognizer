<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblrevenuegrowthpotentialTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tblrevenuegrowthpotential', function(Blueprint $table)
		{
			$table->increments('growthpotentialid');
			$table->integer('entity_id');
			$table->integer('loginid')->index();
			$table->integer('current_market');
			$table->integer('new_market');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tblrevenuegrowthpotential');
	}

}
