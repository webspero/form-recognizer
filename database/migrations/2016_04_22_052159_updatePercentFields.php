<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePercentFields extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::statement('ALTER TABLE tblmajorcustomer MODIFY customer_share_sales FLOAT');
		DB::statement('ALTER TABLE tblmajorsupplier MODIFY supplier_share_sales FLOAT');
		DB::statement('ALTER TABLE tblbusinessdriver MODIFY businessdriver_total_sales FLOAT');
		DB::statement('ALTER TABLE tblowner MODIFY percent_of_ownership FLOAT');
		DB::statement('ALTER TABLE tblservicesoffer MODIFY servicesoffer_share_revenue FLOAT');
		DB::statement('ALTER TABLE tblshareholders MODIFY percentage_share FLOAT');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::statement('ALTER TABLE tblmajorcustomer MODIFY customer_share_sales INT');
		DB::statement('ALTER TABLE tblmajorsupplier MODIFY supplier_share_sales INT');
		DB::statement('ALTER TABLE tblbusinessdriver MODIFY businessdriver_total_sales INT');
		DB::statement('ALTER TABLE tblowner MODIFY percent_of_ownership INT');
		DB::statement('ALTER TABLE tblservicesoffer MODIFY servicesoffer_share_revenue INT');
		DB::statement('ALTER TABLE tblshareholders MODIFY percentage_share INT');
	}

}
