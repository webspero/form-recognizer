<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankListTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tblbank', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('bank_name');
			$table->string('bank_type');
			$table->string('contact_person');
			$table->string('contact_person_designation');
			$table->string('bank_street_address1');
			$table->string('bank_street_address2');
			$table->string('bank_city');
			$table->string('bank_no_of_offices');
			$table->string('bank_phone');
			$table->string('bank_email');
			$table->string('bank_website');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tblbank');
	}

}
