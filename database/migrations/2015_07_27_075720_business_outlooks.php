<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BusinessOutlooks extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('business_outlooks', function(Blueprint $table)
        {
            $table->increments('business_outlook_id');
            $table->integer('industry_id');
            $table->decimal('outlook_idx_q1', 8, 2);
            $table->decimal('outlook_idx_q2', 8, 2);
            $table->decimal('outlook_idx_q3', 8, 2);
            $table->decimal('outlook_idx_q4', 8, 2);
            $table->decimal('outlook_idx_q5', 8, 2);
            $table->decimal('outlook_idx_q6', 8, 2);
            $table->decimal('outlook_idx_q7', 8, 2);
            $table->decimal('outlook_idx_q8', 8, 2);
            $table->tinyInteger('status');
            $table->date('date_revised');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('business_outlooks');
	}

}
