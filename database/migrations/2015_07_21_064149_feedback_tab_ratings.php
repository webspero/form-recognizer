<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FeedbackTabRatings extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('feedback_tab_ratings', function(Blueprint $table)
        {
            $table->increments('tab_rating_id');
            $table->integer('transaction_id');
            $table->tinyInteger('tab_id');
            $table->tinyInteger('ignored');
            $table->text('user_difficulty');
            $table->text('user_recommendation');
            $table->tinyInteger('user_rating');
            $table->tinyInteger('status');
            $table->date('date_rated');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('feedback_tab_ratings');
	}

}
