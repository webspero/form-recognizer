<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBalanceSheetsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('fin_balance_sheets', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('financial_report_id');
			$table->smallInteger('index_count');
			$table->decimal('PropertyPlantAndEquipment', 15, 2);
			$table->decimal('InvestmentProperty', 15, 2);
			$table->decimal('Goodwill', 15, 2);
			$table->decimal('IntangibleAssetsOtherThanGoodwill', 15, 2);
			$table->decimal('InvestmentAccountedForUsingEquityMethod', 15, 2);
			$table->decimal('InvestmentsInSubsidiariesJointVenturesAndAssociates', 15, 2);
			$table->decimal('NoncurrentBiologicalAssets', 15, 2);
			$table->decimal('NoncurrentReceivables', 15, 2);
			$table->decimal('NoncurrentInventories', 15, 2);
			$table->decimal('DeferredTaxAssets', 15, 2);
			$table->decimal('CurrentTaxAssetsNoncurrent', 15, 2);
			$table->decimal('OtherNoncurrentFinancialAssets', 15, 2);
			$table->decimal('OtherNoncurrentNonfinancialAssets', 15, 2);
			$table->decimal('NoncurrentNoncashAssetsPledgedAsCollateral', 15, 2);
			$table->decimal('NoncurrentAssets', 15, 2);
			$table->decimal('Inventories', 15, 2);
			$table->decimal('TradeAndOtherCurrentReceivables', 15, 2);
			$table->decimal('CurrentTaxAssetsCurrent', 15, 2);
			$table->decimal('CurrentBiologicalAssets', 15, 2);
			$table->decimal('OtherCurrentFinancialAssets', 15, 2);
			$table->decimal('OtherCurrentNonfinancialAssets', 15, 2);
			$table->decimal('CashAndCashEquivalents', 15, 2);
			$table->decimal('CurrentNoncashAssetsPledgedAsCollateral', 15, 2);
			$table->decimal('NoncurrentAssetsOrDisposalGroups', 15, 2);
			$table->decimal('CurrentAssets', 15, 2);
			$table->decimal('Assets', 15, 2);
			$table->decimal('IssuedCapital', 15, 2);
			$table->decimal('SharePremium', 15, 2);
			$table->decimal('TreasuryShares', 15, 2);
			$table->decimal('OtherEquityInterest', 15, 2);
			$table->decimal('OtherReserves', 15, 2);
			$table->decimal('RetainedEarnings', 15, 2);
			$table->decimal('NoncontrollingInterests', 15, 2);
			$table->decimal('Equity', 15, 2);
			$table->decimal('NoncurrentProvisionsForEmployeeBenefits', 15, 2);
			$table->decimal('OtherLongtermProvisions', 15, 2);
			$table->decimal('NoncurrentPayables', 15, 2);
			$table->decimal('DeferredTaxLiabilities', 15, 2);
			$table->decimal('CurrentTaxLiabilitiesNoncurrent', 15, 2);
			$table->decimal('OtherNoncurrentFinancialLiabilities', 15, 2);
			$table->decimal('OtherNoncurrentNonfinancialLiabilities', 15, 2);
			$table->decimal('NoncurrentLiabilities', 15, 2);
			$table->decimal('CurrentProvisionsForEmployeeBenefits', 15, 2);
			$table->decimal('OtherShorttermProvisions', 15, 2);
			$table->decimal('TradeAndOtherCurrentPayables', 15, 2);
			$table->decimal('CurrentTaxLiabilitiesCurrent', 15, 2);
			$table->decimal('OtherCurrentFinancialLiabilities', 15, 2);
			$table->decimal('OtherCurrentNonfinancialLiabilities', 15, 2);
			$table->decimal('LiabilitiesIncludedInDisposalGroups', 15, 2);
			$table->decimal('CurrentLiabilities', 15, 2);
			$table->decimal('Liabilities', 15, 2);
			$table->decimal('EquityAndLiabilities', 15, 2);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('fin_balance_sheets');
	}

}
