<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableOpenSanctions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('open_sanctions', function(Blueprint $table)
      {
        $table->text('name')->nullable()->after('id');
        $table->text('country')->nullable();
        $table->text('fr_citation')->nullable();
        $table->text('firstname')->nullable()->change();
        $table->text('lastname')->nullable()->change();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('open_sanctions', function(Blueprint $table)
		{
			$table->dropColumn('name');
			$table->dropColumn('country');
			$table->dropColumn('fr_citation');
		});
    }
}
