<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTblservicesofferTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tblservicesoffer', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `tblservicesoffer` MODIFY `servicesoffer_seasonality` smallint NOT NULL;');
			DB::statement('ALTER TABLE `tblservicesoffer` MODIFY `servicesoffer_share_revenue` smallint NOT NULL;');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblservicesoffer', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `tblservicesoffer` MODIFY `servicesoffer_seasonality` varchar(200) NOT NULL;');
			DB::statement('ALTER TABLE `tblservicesoffer` MODIFY `servicesoffer_share_revenue` varchar(200) NOT NULL;');
		});
	}

}
