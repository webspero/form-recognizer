<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateInvestigatorNotesIsDone extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('investigator_notes', function(Blueprint $table)
		{
			$table->smallInteger('status')->default(0)->after('others_docs');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('investigator_notes', function(Blueprint $table)
		{
			$table->dropColumn('status');
		});
	}

}
