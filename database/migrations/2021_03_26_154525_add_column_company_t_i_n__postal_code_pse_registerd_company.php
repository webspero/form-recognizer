<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnCompanyTINPostalCodePseRegisterdCompany extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pse_registered_companies', function (Blueprint $table) {
            $table->string('company_tin', 255)->nullable();
            $table->string('postal_code', 255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pse_registered_companies', function (Blueprint $table) {
            $table->dropColumn('company_tin');
            $table->dropColumn('postal_code');
        });
    }
}
