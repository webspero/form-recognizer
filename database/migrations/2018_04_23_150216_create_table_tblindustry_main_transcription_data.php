<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTblindustryMainTranscriptionData extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblindustry_main_transcription_data', function(Blueprint $table)
        {
            $table->integer('tblindustry_main_id')->primary();
            $table->float('gross_revenue_growth', 10, 2)->default(0);
            $table->float('net_income_growth', 10, 2)->default(0);
            $table->float('gross_profit_margin', 10, 2)->default(0);
            $table->float('net_profit_margin', 10, 2)->default(0);
            $table->float('net_cash_margin', 10, 2)->default(0);
            $table->float('current_ratio', 10, 2)->default(0);
            $table->float('debt_equity_ratio', 10, 2)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }

}
