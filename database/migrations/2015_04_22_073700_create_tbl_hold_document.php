<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblHoldDocument extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tblholddocument', function(Blueprint $table)
		{
			$table->increments('holddocumentid');
			$table->integer('loginid')->index();
			$table->integer('investigatorid')->index();
			$table->string('document_name',200);
			$table->string('document_rename',200);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tblholddocument');
	}

}
