<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTblbranchesPhoneEmail extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tblbranches', function(Blueprint $table)
		{
			$table->string('branch_phone', 128)->nullable()->after('branch_contact_details');
			$table->string('branch_email')->nullable()->after('branch_contact_details');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblbranches', function(Blueprint $table)
		{
			$table->dropColumn('branch_phone');
			$table->dropColumn('branch_email');
		});
	}

}
