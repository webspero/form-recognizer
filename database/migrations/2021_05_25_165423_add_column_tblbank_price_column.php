<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnTblbankPriceColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tblbank', function (Blueprint $table) {
            $table->integer('simplified_price')->default(3500);
            $table->integer('standalone_price')->default(3500);
            $table->integer('premium_zone1_price')->default(4300);
            $table->integer('premium_zone2_price')->default(5300);
            $table->integer('premium_zone3_price')->default(6300);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
