<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePseEdgeList extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pse_registered_companies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email_address');
            $table->string('website');
            $table->string('tel_num');
            $table->string('business_address');
            $table->text('company_description');
            $table->string('sector');
            $table->string('sub_sector');
            $table->smallInteger('corporate_life');
            $table->dateTime('incorporation_date');
            $table->smallInteger('num_directors');
            $table->string('source');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pse_registered_companies');
    }
}
