<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PasswordRequests extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('password_requests', function(Blueprint $table)
        {
            $table->increments('request_id');
            $table->integer('user_id');
            $table->string('notification_code', 8);
            $table->tinyInteger('status');
            $table->date('date_requested');
            $table->date('date_used');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('password_requests');
	}

}
