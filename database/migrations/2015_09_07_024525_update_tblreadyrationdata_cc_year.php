<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTblreadyrationdataCcYear extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tblreadyrationdata', function(Blueprint $table)
		{
			$table->integer('cc_year1')->after('debt_equity_ratio2');
			$table->integer('cc_year2')->after('cc_year1');
			$table->integer('cc_year3')->after('cc_year2');
			$table->integer('receivables_turnover3')->after('receivables_turnover2');
			$table->integer('inventory_turnover3')->after('inventory_turnover2');
			$table->integer('accounts_payable_turnover3')->after('accounts_payable_turnover2');
			$table->integer('cash_conversion_cycle3')->after('cash_conversion_cycle2');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblreadyrationdata', function(Blueprint $table)
		{
			$table->dropColumn('cc_year1');
			$table->dropColumn('cc_year2');
			$table->dropColumn('cc_year3');
			$table->dropColumn('receivables_turnover3');
			$table->dropColumn('inventory_turnover3');
			$table->dropColumn('accounts_payable_turnover3');
			$table->dropColumn('cash_conversion_cycle3');
		});
	}

}
