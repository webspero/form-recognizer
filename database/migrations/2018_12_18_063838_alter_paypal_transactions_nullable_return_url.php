<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPaypalTransactionsNullableReturnUrl extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::statement('ALTER TABLE `paypal_transactions` MODIFY `client_return` VARCHAR(250) NULL;');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::statement('ALTER TABLE `paypal_transactions` MODIFY `client_return` VARCHAR(250) NOT NULL;');
	}

}
