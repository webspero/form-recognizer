<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BatchUpdateTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// Useless tables
		Schema::drop('tblbankproducts');
		Schema::drop('tblbankruphistory');
		Schema::drop('tblcommunityreputation');
		Schema::drop('tblcreditfacilitiesreasons');
		Schema::drop('tblcreditlines');
		Schema::drop('tblcreditlines_investigation');
		Schema::drop('tblemployee');
		Schema::drop('tblhold');
		Schema::drop('tblholddocument');
		Schema::drop('tblincomeandexpenditure');
		Schema::drop('tblinvestigation');
		Schema::drop('tblmajorcustomer_sold');
		Schema::drop('tblmajorsupplier_purchase');
		Schema::drop('tblmanagementturnover');
		Schema::drop('tblnetworkinfluence');
		Schema::drop('tblpreviousloanapplication');
		Schema::drop('tbltrackrecord');

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
