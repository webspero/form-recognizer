<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblCertifications extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tblcertifications', function(Blueprint $table)
		{
			$table->increments('certificationid');
			$table->integer('entity_id');
			$table->integer('loginid')->index();
			$table->string('certification_details',200);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tblcertifications');
	}

}
