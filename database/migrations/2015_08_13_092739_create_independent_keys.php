<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIndependentKeys extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('independent_keys', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('email');
			$table->string('serial_key');
			$table->smallInteger('batch');
			$table->smallInteger('is_paid')->default(0);
			$table->smallInteger('is_used')->default(0);
			$table->integer('login_id')->default(0);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('independent_keys');
	}

}
