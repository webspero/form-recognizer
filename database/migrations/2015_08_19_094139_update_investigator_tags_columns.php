<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateInvestigatorTagsColumns extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('investigator_tag', function(Blueprint $table)
		{
			$table->string('group_name')->after('item');
			$table->string('label')->after('group_name');
			$table->string('value')->after('label');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('investigator_tag', function(Blueprint $table)
		{
			$table->dropColumn('group_name');
			$table->dropColumn('label');
			$table->dropColumn('value');
		});
	}

}
