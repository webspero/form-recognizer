<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddQuestionTypeBankEntity extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tbllogin', function(Blueprint $table)
		{
			$table->dropColumn('question_type');
            $table->dropColumn('ecf_included');
			$table->dropColumn('rcl_included');
		});
        
        Schema::table('supervisor_application', function(Blueprint $table)
		{
			$table->dropColumn('question_type');
		});
        
        Schema::create('questionnaire_config', function(Blueprint $table)
		{
            $table->increments('questionnaire_id');
			$table->integer('bank_id');
			$table->tinyInteger('question_type')->default(0);
			$table->tinyInteger('affiliates')->default(0);
			$table->tinyInteger('products')->default(0);
			$table->tinyInteger('main_locations')->default(0);
			$table->tinyInteger('capital_details')->default(0);
			$table->tinyInteger('branches')->default(0);
			$table->tinyInteger('import_export')->default(0);
			$table->tinyInteger('insurance')->default(0);
			$table->tinyInteger('cash_conversion')->default(0);
			$table->tinyInteger('major_customer')->default(1);
			$table->tinyInteger('major_supplier')->default(1);
			$table->tinyInteger('major_market')->default(0);
			$table->tinyInteger('competitors')->default(0);
			$table->tinyInteger('business_drivers')->default(1);
			$table->tinyInteger('customer_segments')->default(1);
			$table->tinyInteger('past_projects')->default(0);
			$table->tinyInteger('future_growth')->default(1);
			$table->tinyInteger('owner_details')->default(1);
			$table->tinyInteger('directors')->default(0);
			$table->tinyInteger('shareholders')->default(0);
            $table->tinyInteger('ecf')->default(0);
			$table->tinyInteger('company_succession')->default(1);
			$table->tinyInteger('landscape')->default(0);
			$table->tinyInteger('economic_factors')->default(0);
			$table->tinyInteger('tax_payments')->default(0);
			$table->tinyInteger('risk_assessment')->default(1);
			$table->tinyInteger('growth_potential')->default(0);
			$table->tinyInteger('investment_expansion')->default(0);
            $table->tinyInteger('rcl')->default(0);
			$table->integer('modified_by');
			$table->datetime('created_at');
            $table->datetime('updated_at');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tbllogin', function(Blueprint $table)
		{
			$table->tinyInteger('question_type')->after('role')->default(0);
			$table->tinyInteger('ecf_included')->after('question_type')->default(1);
			$table->tinyInteger('rcl_included')->after('ecf_included')->default(1);
		});
        
        Schema::table('supervisor_application', function(Blueprint $table)
		{
			$table->tinyInteger('ecf_included')->after('question_type')->default(1);
			$table->tinyInteger('rcl_included')->after('ecf_included')->default(1);
		});
        
        Schema::drop('questionnaire_config');
	}

}
