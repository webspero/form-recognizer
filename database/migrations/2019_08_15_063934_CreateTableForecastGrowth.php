<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableForecastGrowth extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		//
		Schema::create('growth_forecast_results', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('main_id');
			$table->integer('year');
			$table->float('Q1');
			$table->float('Q2');
			$table->float('Q3');
			$table->float('Q4');
			$table->float('Q5');
			$table->float('Q6');
			$table->float('Q7');
			$table->float('Q8');
			$table->float('Q9');
			$table->float('Q10');
			$table->float('Q11');
			$table->float('Q12');
			$table->float('Q13');
			$table->float('Q14');
			$table->float('Q15');
			$table->float('Q16');
			$table->float('gf_year1');
			$table->float('gf_year2');
			$table->float('gf_year3');
			$table->float('is_deleted');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop("growth_forecast_results");
	}

}
