<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableforAutomateStandaloneRating extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('automate_standalone_rating_logs', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string("data", 250);
			$table->integer('entity_id');
			$table->integer('status');
			$table->string('error',100)->nullable();
			$table->timestamps();
			$table->integer("is_deleted");
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop("automate_standalone_rating_logs");
	}

}
