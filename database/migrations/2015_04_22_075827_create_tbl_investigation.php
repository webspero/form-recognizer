<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblInvestigation extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tblinvestigation', function(Blueprint $table)
		{
			$table->increments('investigationid');
			$table->integer('loginid')->index();
			$table->integer('entityid')->index();
			$table->date('start_date')->nullable();
			$table->date('end_date');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tblinvestigation');
	}

}
