<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateEntityTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tblentity', function($table)
		{
			$table->date('sec_reg_date');
			$table->string('sec_cert', 45);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tblentity', function($table)
		{
			$table->dropColumn('sec_reg_date');
			$table->dropColumn('sec_cert');
		});
	}

}
