<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePseAnnualReports extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('pse_balance_sheets', function($table)
		{
			$table->string('period', 100);
		});
        
        Schema::table('pse_income_statement', function($table)
		{
			$table->string('period', 100);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pse_balance_sheets', function($table)
		{
			$table->dropColumn('period');
		});
        
        Schema::table('pse_income_statement', function($table)
		{
			$table->dropColumn('period');
		});
	}

}
