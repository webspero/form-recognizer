<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTbltrackrecordTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tbltrackrecord', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `tbltrackrecord` MODIFY `bus_owner` varchar(100) NOT NULL;');
			DB::statement('ALTER TABLE `tbltrackrecord` MODIFY `bus_spouse` varchar(100) NOT NULL;');
			DB::statement('ALTER TABLE `tbltrackrecord` MODIFY `bus_relative` varchar(100) NOT NULL;');
			DB::statement('ALTER TABLE `tbltrackrecord` MODIFY `bus_profmanager` varchar(100) NOT NULL;');
			DB::statement('ALTER TABLE `tbltrackrecord` MODIFY `bus_other` varchar(100) NOT NULL;');
			DB::statement('ALTER TABLE `tbltrackrecord` MODIFY `bus_other_specify` varchar(100) NOT NULL;');
			DB::statement('ALTER TABLE `tbltrackrecord` MODIFY `hmr_status` varchar(100) NOT NULL;');
			DB::statement('ALTER TABLE `tbltrackrecord` MODIFY `exposure_position` varchar(100) NOT NULL;');
			DB::statement('ALTER TABLE `tbltrackrecord` MODIFY `exposure_type` varchar(100) NOT NULL;');
			DB::statement('ALTER TABLE `tbltrackrecord` MODIFY `cr_position` varchar(100) NOT NULL;');
			DB::statement('ALTER TABLE `tbltrackrecord` MODIFY `cr_type` varchar(100) NOT NULL;');
			DB::statement('ALTER TABLE `tbltrackrecord` MODIFY `dr_position` varchar(100) NOT NULL;');
			DB::statement('ALTER TABLE `tbltrackrecord` MODIFY `dr_type` varchar(100) NOT NULL;');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tbltrackrecord', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `tbltrackrecord` MODIFY `bus_owner` varchar(200) NOT NULL;');
			DB::statement('ALTER TABLE `tbltrackrecord` MODIFY `bus_spouse` varchar(200) NOT NULL;');
			DB::statement('ALTER TABLE `tbltrackrecord` MODIFY `bus_relative` varchar(200) NOT NULL;');
			DB::statement('ALTER TABLE `tbltrackrecord` MODIFY `bus_profmanager` varchar(200) NOT NULL;');
			DB::statement('ALTER TABLE `tbltrackrecord` MODIFY `bus_other` varchar(200) NOT NULL;');
			DB::statement('ALTER TABLE `tbltrackrecord` MODIFY `bus_other_specify` varchar(200) NOT NULL;');
			DB::statement('ALTER TABLE `tbltrackrecord` MODIFY `hmr_status` varchar(200) NOT NULL;');
			DB::statement('ALTER TABLE `tbltrackrecord` MODIFY `exposure_position` varchar(200) NOT NULL;');
			DB::statement('ALTER TABLE `tbltrackrecord` MODIFY `exposure_type` varchar(200) NOT NULL;');
			DB::statement('ALTER TABLE `tbltrackrecord` MODIFY `cr_position` varchar(200) NOT NULL;');
			DB::statement('ALTER TABLE `tbltrackrecord` MODIFY `cr_type` varchar(200) NOT NULL;');
			DB::statement('ALTER TABLE `tbltrackrecord` MODIFY `dr_position` varchar(200) NOT NULL;');
			DB::statement('ALTER TABLE `tbltrackrecord` MODIFY `dr_type` varchar(200) NOT NULL;');
		});
	}

}
