<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableIndustryClass extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('industry_class', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string("name", 250);
			$table->integer('main_id');
			$table->integer('status');
			$table->timestamps();
			$table->integer("is_deleted");
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop("industry_class");
	}

}
