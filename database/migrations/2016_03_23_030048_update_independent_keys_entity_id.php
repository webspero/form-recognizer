<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateIndependentKeysEntityId extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('independent_keys', function(Blueprint $table)
		{
			$table->integer('entity_id')->after('login_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('independent_keys', function(Blueprint $table)
		{
			$table->dropColumn('entity_id');
		});
	}

}
