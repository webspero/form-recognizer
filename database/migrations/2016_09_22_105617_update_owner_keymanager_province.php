<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateOwnerKeymanagerProvince extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::statement('ALTER TABLE `tblkeymanagers` MODIFY `province` VARCHAR(128) NULL;');
		DB::statement('ALTER TABLE `tblowner` MODIFY `province` VARCHAR(128) NULL;');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::statement('ALTER TABLE `tblkeymanagers` MODIFY `province` SMALLINTEGER(6) NULL;');
		DB::statement('ALTER TABLE `tblowner` MODIFY `province` SMALLINTEGER(6) NULL;');
	}

}
